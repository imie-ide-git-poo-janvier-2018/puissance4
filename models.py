#-*- coding: utf-8 -*-
class Grille:
    def __init__(self, nbcol, nbline):
        self.nb_col = nbcol
        self.nb_line = nbline
        self.column_list = [ [] for i in range(self.nb_col) ]

    def can_put_token(self, column):
        return column >= 0 and column < self.nb_col and  len(self.column_list[column]) < self.nb_line

    def __str__(self):
        ret = "\n"
        for i in range(self.nb_line-1, -1, -1):
            ret += "%s "%i
            for col in self.column_list:
                if len(col) <= i:
                    ret += " "
                else:
                    ret += str(col[i])
            ret += "\n"
        return ret

    def verify_winner_or_full(self):
        pass
    
class Jeton:
    """
    The the Jeton class
    """
    def __init__(self, joueur):
        """
        This is the contructor
        """
        self.couleur = ""
        self.position = None
        self.joueur = joueur
    
    def __str__(self):
        return self.joueur.token_character

class Joueur:
    def __init__(self, pseudo, token_character):
        self.pseudo = pseudo
        self.token_character = token_character
        
    def __str__(self):
        return self.pseudo

        
class Jeu:
    def __init__(self, joueur1, joueur2, grille):
        self.nb_jeton = 0
        self.joueur1 = joueur1
        self.joueur2 = joueur2
        self.grille = grille
        self.current_player = None

    def ask_for_column(self):
        question = "%s select column between 1 and %s\n"%(self.current_player, self.grille.nb_col)
        retour_question = input(question)
        try:
            col = int(retour_question)
        except ValueError as e:
            print("%s! Vous avez entré un truc bizar"%self.current_player)
            return True
        if self.grille.can_put_token(col):
            jeton = Jeton(self.current_player)
            self.grille.column_list[col].append(jeton)
            return False
        else:
            return True
        
        
    def game_is_finished(self):
        return False
    
    def start(self):

        while not self.game_is_finished():
            if self.current_player in [None, self.joueur2]:
                self.current_player = self.joueur1
            else:
                self.current_player = self.joueur2
            #-- player one select column
            while self.ask_for_column() :
                pass

            print(self.grille)

            self.grille.verify_winner_or_full()
                
