# coding: utf-8
from models import Grille, Jeu, Jeton, Joueur

if __name__ == "__main__":
    #-- ask for grille size
    nb_col = int(input("nb_col?\n"))
    nb_line = int(input("nb_lin?\n"))

    #--  create grille instance
    grille = Grille(nb_col, nb_line)

    #-- ask for players pseudos
    pseudo_j1 = input("pseudo J1?\n")
    pseudo_j2 = input("pseudo J2?\n")
    
    #-- instanciate players
    j1 = Joueur(pseudo_j1, "X")
    j2 = Joueur(pseudo_j2, "0")

    #-- debug
    print(grille)
    print(j1)
    print(j2)

    jeu = Jeu(j1, j2, grille)

    print(jeu)

    jeu.start()
